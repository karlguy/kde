# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE EDU: Vocabulary Trainer"
DESCRIPTION="It helps you to memorize your vocabulary, for example when you are trying to learn a
foreign language. It supports many language specific features, but can be used for other learning
tasks as well. It uses the spaced repetition learning method, which makes learning optimal.
Vocabulary collections can be downloaded by \"Get Hot New Stuff\" or created with the built-in
editor."

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="html-export"

KF5_MIN_VER=5.22.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libkeduvocdocument:${SLOT}
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kross:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.2.0]
        x11-libs/qtmultimedia:5[>=5.2.0]
        x11-libs/qtsvg:5[>=5.2.0]
        x11-libs/qtwebengine:5[>=5.2.0]
        html-export? (
            dev-libs/libxml2:2.0
            dev-libs/libxslt
        )
    recommendation:
        kde/kdeedu-data:${SLOT} [[ description = [ Install additional vocabularies ] ]]
    suggestion:
        dev-lang/python:* [[
            description = [ Fetch sound files from Wikipedia/Wiktionary ]
        ]]
"
# currently commented out
# kde-frameworks/attica:5[>=${KF5_MIN_VER}]
# kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'html-export LibXml2'
    'html-export LibXslt'
)

parley_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

parley_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

