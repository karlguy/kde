# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Collection of cli tools for KDE"

LICENCES="
    Artistic [[ note = [ kdesu ] ]]
    GPL-2
    LGPL-2.1 [[ note = [ kioclient ] ]]
"
SLOT="4"
MYOPTIONS=""

KF5_MIN_VER=5.42.0
QT_MIN_VER=5.9.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdesu:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        !kde/kde-runtime[<15.12.1-r1] [[
            description = [ File collisions between ported and unported apps ]
            resolution = uninstall-blocked-after
        ]]
"

if ever at_least 5.12.90 ; then
    DEPENDENCIES+="
        build+run:
            kde/plasma-workspace:4[>=${PV}] [[ note = [ LibKWorkspace ] ]]
    "
fi

# Tries to connect to X (5.1.0)
RESTRICT=test

