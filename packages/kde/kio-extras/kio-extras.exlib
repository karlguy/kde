# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] test-dbus-daemon
require freedesktop-desktop freedesktop-mime

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Colletion of Input/Output protocols for KDE"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
MYOPTIONS="
    audio-thumbnail [[ description = [ Thumbnails for audio files from embedded album covers ] ]]
    htmlthumbnail   [[ description = [ Generate thumbnails of HTML pages via QtWebEngine ] ]]
    mtp     [[ description = [ Support for the mtp:/ kioslave accessing MTP devices ] ]]
    openexr [[ description = [ Support for OpenEXR thumbnails ] ]]
    samba   [[ description = [ Support for the smb:/ kioslave for MS windows shares ] ]]
    sftp    [[ description = [ Support for the sftp:/ kioslave by libssh ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER=5.40.0
QT_MIN_VER=5.5.0

DEPENDENCIES="
    build:
        dev-util/gperf [[ note = [ man kioslave ] ]]
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        mtp? ( virtual/pkg-config )
        samba? ( virtual/pkg-config )
    build+run:
        kde-frameworks/kactivities:5[>=5.20] [[ note = [ possibly optional ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}] [[ note = [ man kioslave ] ]]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpty:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.6.60][qt5(-)]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info[>=0.40]
        audio-thumbnail? ( media-libs/taglib[>=1.11] )
        htmlthumbnail? ( x11-libs/qtwebengine:5[>=5.7.0] )
        mtp? (
            media-libs/libmtp[>=1.1.2]
            !kde/kio-mtp [[
                description = [ kio-mtp was merge into kio-extras ]
                resolution = uninstall-blocked-after
            ]]
        )
        openexr? ( media-libs/openexr )
        samba? ( net-fs/samba:= )
        sftp? ( net-libs/libssh[>=0.6.0] )
        !kde/kde-runtime[<15.12.1-r1] [[
            description = [ File collisions between ported and unported apps ]
            resolution = uninstall-blocked-after
        ]]
        !kde-frameworks/kactvities:5[<5.20.0] [[
            description = [ Parts of kactivities have been moved to kio-extras ]
            resolution = uninstall-blocked-after
        ]]
        !kde-frameworks/kio:5[<5.3.0] [[
            description = [ kio_trash was moved from kio-extras to kio ]
            resolution = uninstall-blocked-after
        ]]
    run:
        mtp? ( kde/kde-cli-tools:4 [[ note = [ kioclient5 ] ]] )
"

# 1 of 1 test tries to start kdeinit which needs X
RESTRICT=test

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'audio-thumbnail Taglib'
    'htmlthumbnail Qt5WebEngineWidgets'
    Mtp
    OpenEXR
    Samba
    'sftp LibSSH'
)

kio-extras_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

kio-extras_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

