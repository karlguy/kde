# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Library for common KDE PIM applications"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    designer [[ description = [ Install Qt Designer plugins ] ]]
"

KF5_MIN_VER=5.44.0
QT_MIN_VER=5.8.0

DEPENDENCIES="
    build+run:
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-search:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=${PV}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        designer? ( kde-frameworks/kdesignerplugin:5[>=${KF5_MIN_VER}] )
        !kde/kdepim[<16.03.80] [[
            description = [ libkdepim has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
"

# 11 of 11 tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'designer KF5DesignerPlugin' )

