# Copyright 2013-2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least scm ; then
    SCM_REPOSITORY="git://anongit.kde.org/${PN}.git"
    require scm-git
    DOWNLOADS=""
else
    DOWNLOADS="mirror://kde/stable/frameworks/$(ever range -2)/${PNV}.tar.xz"
    UPSTREAM_RELEASE_NOTES="http://kde.org/announcements/kde-frameworks-${PV}.php"
fi

require cmake [ api=2 ]

export_exlib_phases src_prepare src_install

SUMMARY="Extra modules and scripts for CMake"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES+="
    build:
        doc? (
            dev-python/Sphinx[>=1.2]
            x11-libs/qttools:5 [[ note = [ QCollectionGenerator ] ]]
        )
    test:
        x11-libs/qtbase:5
        x11-libs/qttools:5 [[ note = [ Qt5Linguist ] ]]
    suggestion:
        dev-util/clazy [[ description = [ Richer warnings for Qt-related code with ENABLE_CLAZY ] ]]
"
# NOTE: There's also an appstream test which is forced on every project, needs
# appstream-cli from https://www.freedesktop.org/wiki/Distributions/AppStream/,
# which is is unpackaged and appstream files are not really interesting to us.
# So we just skip the test by not having appstream-cli.

# TODO: Tests break install target
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'doc HTML_DOCS'
    'doc MAN_DOCS'
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_QTHELP_DOCS:BOOL=FALSE
    -DSHARE_INSTALL_DIR:PATH=/usr/share
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

extra-cmake-modules_src_prepare() {
    cmake_src_prepare

    # Fix 4 failing tests. ctest calls cmake which doesn't know anything about
    # our params from cmake.exlib.
    # https://bugs.kde.org/show_bug.cgi?id=349028
    edo sed -e "/BUILD_TESTING:BOOL=/s/ON/ON -DCMAKE_AR:PATH=${AR} -DCMAKE_RANLIB:PATH=${RANLIB}/" \
            -i tests/ECMAddTests/CMakeLists.txt
}

extra-cmake-modules_src_install() {
    default

    hereenvd 45kf5 <<EOF
XDG_CONFIG_DIRS=/etc/xdg
XDG_DATA_DIRS=/usr/share
EOF
}

