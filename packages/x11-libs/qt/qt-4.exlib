# Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2008-2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-4.3.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require qt [ pn='qt-everywhere-opensource-src' ] freedesktop-desktop flag-o-matic

export_exlib_phases src_prepare src_configure src_compile src_install

myexparam pn=${MY_PN:-${PN}}
myexparam pv=${MY_PV:-${PV}}
MY_PNV=$(exparam pn)-$(exparam pv)

SUMMARY="Qt Cross-platform application framework for desktop and embedded development"

LICENCES="|| ( LGPL-2.1 GPL-3 )"

MYOPTIONS_PARTS="demos doc examples" # tools
SQL_BACKENDS="mysql postgresql sqlite" # firebird odbc
# accessibility gif jpeg mng png svg enabled by default
MYOPTIONS="dbus cups glib opengl phonon
    qt3support sql tiff webkit
    ${MYOPTIONS_PARTS}
    ( ${SQL_BACKENDS} ) [[ requires = sql ]]
    sql? (
        ( ${SQL_BACKENDS} ) [[ number-selected = at-least-one ]]
    )
    demos       [[ description = [ Compile demo-programs that show off Qt's widgets & functionality ] ]]
    glib        [[ description = [ Add support for the glib eventloop ] ]]
    gtk         [[ description = [ Enable GTK+ style support, this will install a Qt4 style that renders using GTK+, to blend in with a GTK+ desktop ]
                   requires = glib ]]
    multimedia  [[ description = [ Build the QtMultimedia module ] ]]
    phonon      [[ description = [ Multimedia API with support for multiple backends for playback ] ]]
    pulseaudio
    qt3support  [[ description = [ A module consisting of classes that ease porting from Qt3 to Qt4 ] ]]
    sql         [[ description = [ Build the QtSQL module ] ]]
    webkit      [[ description = [ Build WebKit, an open source web browser engine, using Qt rendering ] ]]

    ( platform: amd64 x86 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

MYOPTIONS+="
    platform: amd64 x86
    amd64_cpu_features:
        3dnow
        sse3
        ssse3   [[ requires = [ amd64_cpu_features: sse3   ] ]]
        sse4.1
        sse4.2
        avx
    arm_cpu_features: neon
    x86_cpu_features:
        3dnow
        mmx
        sse
        sse2
        sse3
        ssse3   [[ requires = [ x86_cpu_features: sse3   ] ]]
        sse4.1
        sse4.2
        avx
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/icu:=
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libmng
        media-libs/libpng:=
        sys-libs/zlib
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXrandr[>=1.1]
        x11-libs/libXrender[>=0.6]
        x11-libs/libXv

        cups? ( net-print/cups )
        dbus? ( sys-apps/dbus[>=1.0.2] )
        glib? ( dev-libs/glib:2 )
        gtk? (
            x11-libs/gtk+:2[>=2.10]
            dev-libs/atk
        )
        multimedia? ( sys-sound/alsa-lib )
        mysql? ( virtual/mysql )
        opengl? ( x11-dri/mesa )
        postgresql? ( dev-db/postgresql )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.10] )
        sqlite? ( dev-db/sqlite:3 )
        tiff? ( media-libs/tiff )
    post:
        phonon? ( media-libs/phonon[>=4.3.80][qt4(+)] )
"

# Packages that need QtSQL should depend on x11-libs/qt:4[sql]
# option='sql' needs at least one SQL plugin, any-of mysql, postgresql, sqlite, (firebird, odbc)

qconf4() {
    local x myarch myconf=() host=$(exhost --target) option_to_arch_map=(
        #alpha arm mips s390 sparc
        #{hppa,sh}:generic
        x86_64-pc-linux-gnu:x86_64
        #ia64
        #ppc:powerpc
        #ppc64:powerpc
        i686-pc-linux-gnu:i386
    )
    for x in "${option_to_arch_map[@]}"; do
        if [[ $(exhost --target) == ${x%:*} ]] ; then
            myarch="${x#*:}"
            break
        fi
    done

    [[ -z ${myarch} ]] && die "Your platform is not supported by qt-4.exlib. Please file a bug."

    # paths
    myconf+=(
        -arch ${myarch}
        -platform $(qt_mkspecs_dir)
        -prefix /usr/${host}
        -bindir /usr/${host}/bin
        -docdir /usr/share/doc/${PNVR}
        -headerdir /usr/${host}/include/qt4
        -libdir /usr/${host}/lib/qt4
        -plugindir /usr/${host}/lib/qt4/plugins
        -datadir /usr/${host}/lib/qt4
        -translationdir /usr/share/qt4/translations
        -sysconfdir /etc/qt4
        -examplesdir /usr/share/qt4/examples
        -demosdir /usr/share/qt4/demos
        -importdir /usr/${host}/lib/qt4/imports
        $(exhost --is-native -q || echo "-device-option CROSS_COMPILE=$(exhost --tool-prefix)-")
    )

    edo "${ECONF_SOURCE:-.}"/configure "${myconf[@]}" "$@"
}

qt-4_src_prepare() {
    default

    # gcc:6 compatibility workarounds
    export CXXFLAGS="${CXXFLAGS} -std=gnu++98 -Wno-deprecated"

    # Don't prestrip.
    edo sed -e "/^CONFIG +=/s:$: nostrip:" -i qmake/qmake.pro projects.pro
    edo sed -e "/^QMAKE_SWITCHES=/s:$:CONFIG+=nostrip:" -i configure

    # Don't put libraries in X11R6/.
    edo sed -i -e "s:X11R6/::" "${WORK}"/mkspecs/common/linux.conf

    edo sed -e "s:gcc:${CC}:" \
            -e "s:g++:${CXX}:" \
            -i "${WORK}"/mkspecs/common/g++-base.conf

    edo sed -e "s:QMAKE_CFLAGS_RELEASE.*=.*:QMAKE_CFLAGS_RELEASE=${CPPFLAGS} ${CFLAGS} ${ASFLAGS}:" \
            -e "s:QMAKE_CXXFLAGS_RELEASE.*=.*:QMAKE_CXXFLAGS_RELEASE=${CPPFLAGS} ${CXXFLAGS} ${ASFLAGS}:" \
            -e "s:QMAKE_LFLAGS_RELEASE.*=.*:QMAKE_LFLAGS_RELEASE=${LDFLAGS}:" \
            -e "s:QMAKE_RPATH.*=.*:QMAKE_RPATH=:" \
            -i "${WORK}"/mkspecs/common/g++.conf

    edo sed -e "s:CXXFLAGS.*=:CXXFLAGS=${CPPFLAGS} ${CXXFLAGS} ${ASFLAGS} :" \
            -e "s:LFLAGS.*=:LFLAGS=${LDFLAGS} :" \
            -i "${WORK}"/qmake/Makefile.unix

    edo find config.tests/unix -name '*.test' -type f -print0 | xargs -0 \
            sed -i -e "/bin\/qmake/ s: \"\$SRCDIR/: \
            'QMAKE_CFLAGS+=${CFLAGS}' 'QMAKE_CXXFLAGS+=${CXXFLAGS}' 'QMAKE_LFLAGS+=${LDFLAGS}'&:"

    # Disable the Internet Connection Daemon (ICD) by default (otherwise auto-magic)
    edo sed -e "/^CFG_ICD=/s:auto:no:" -i configure

    # No big chance to come across CoreWLAN on Linux but disable it anyway.
    edo sed -e "/^CFG_COREWLAN=/s:auto:no:" -i configure

    # There's no configure switch for pulseaudio, so we change the default from
    # "auto" to an explicit "yes" or "no".
    if option pulseaudio ; then
        edo sed -e "/^CFG_PULSEAUDIO=/s:auto:yes:" -i configure
    else
        edo sed -e "/^CFG_PULSEAUDIO=/s:auto:no:" -i configure
    fi

    # Disable WebKit tests since they need access to the xserver
    edo sed -e 's|exists($$PWD/WebKit/qt/tests): SUBDIRS += WebKit/qt/tests||' \
            -i "${WORK}"/src/3rdparty/webkit/Source/WebKit.pro
    # Fix WebKit with gcc-4.6, -Werror
    edo sed -e '/QMAKE_CXXFLAGS\s*+=/ s:-Werror::g' \
            -i "${WORK}"/src/3rdparty/webkit/Source/WebKit.pri
    # Fix webkit version number for pkg-config
    edo sed -e 's/^isEmpty(QT_BUILD_TREE)://' \
            -i "${WORK}"/src/3rdparty/webkit/Source/WebKit/qt/QtWebKit.pro

    # Fix the names of some unprefixed executables
    edo sed -e "s: strings : $(exhost --tool-prefix)strings :" \
            -i "${WORK}"/config.tests/unix/endian.test
    edo sed -e "s:^\(QMAKE_AR.*=.\)\(ar.*\):\1$(exhost --tool-prefix)\2:" \
            -i "${WORK}"/mkspecs/common/linux.conf
    edo sed -e "s:\(PKG_CONFIG.*=.\)\(pkg-config\):\1${PKG_CONFIG}:" \
            -i "${WORK}"/mkspecs/features/link_pkgconfig.prf \
            -i "${WORK}"/mkspecs/features/qt_functions.prf
    edo sed -e "s:\(system(\)pkg-config:\1${PKG_CONFIG}:" \
            -i "${WORK}"/src/3rdparty/webkit/Source/JavaScriptCore/wtf/wtf.pri \
            -i "${WORK}"/src/3rdparty/webkit/Source/WebCore/features.pri
    edo sed -e "s:\(readelf\).*-l.*/bin/ls:$(exhost --tool-prefix)\1 -l /usr/$(exhost --target)/bin/ls:" \
            -i "${WORK}"/src/corelib/global/global.pri
}

qt-4_src_configure() {
    local myconf=(
        -confirm-license
        -opensource
        -fast
        -largefile
        -no-rpath
        -no-separate-debug-info
        -pch
        -reduce-relocations
        -release
        -stl
        -verbose
        # For crosscompiling: -force-pkg-config
    )

    myconf+=(
        -DENABLE_VIDEO=0
        -accessibility
        -declarative
        -declarative-debug
        -iconv
        -icu
        -no-egl
        -no-gstreamer
        -no-javascript-jit
        -no-nas-sound
        -no-nis
        -no-phonon-backend
        -no-s60
        -openssl-linked
        -system-proxies
        -system-zlib
        -xinerama
        -xinput
        -xmlpatterns
        $(qt_enable cups)
        $(qt_enable dbus "" "" linked)
        $(qt_enable glib)
        $(qt_enable gtk gtkstyle)
        $(qt_enable multimedia)
        $(qt_enable multimedia audio-backend)
        $(qt_enable opengl)
        $(qt_enable qt3support)
        $(qt_enable phonon)
        $(qt_enable webkit)
    )

    # Avoid auto detection of CPU features:
    # 3dnow, mmx, sse, sse2, sse3, ssse3, sse4.1, sse4.2, avx, iwmmxt, neon
    # Note: iwmmxt breaks build, at least on 4.7
    # Always enable mmx, sse and sse2 on amd64
    if option !platform:amd64 && option !x86_cpu_features:mmx ; then
        myconf+=( -no-mmx )
    fi
    if option !platform:amd64 && option !x86_cpu_features:sse ; then
        myconf+=( -no-sse )
    fi
    if option !platform:amd64 && option !x86_cpu_features:sse2 ; then
        myconf+=( -no-sse2 )
    fi
    for feature in 3dnow sse3 ssse3 sse4.1 sse4.2 avx ; do
        if option !amd64_cpu_features:${feature} && option !x86_cpu_features:${feature} ; then
            myconf+=( -no-${feature} )
            append-flags -mno-${feature}
        fi
    done
    if option !arm_cpu_features:neon ; then
        myconf+=( -no-neon)
    fi

    # X
    local o X_options="fontconfig sm xcursor xfixes xkb xrandr xrender xshape"
    for o in ${X_options}; do
        myconf+=( -${o} )
    done

    myconf+=(
        -no-sql-db2       # IBM DB2 (version 7.1 and above)
        -no-sql-ibase     # Borland InterBase
        -no-sql-oci       # Open Database Connectivity (ODBC) (e.g. MS SQL Server)
        -no-sql-odbc      # Oracle Call Interface Driver
        -no-sql-sqlite2   # SQLite version 2
        -no-sql-symbian   # SQLite version 3 for Symbian SQL Database
        -no-sql-symsql    # SQLite version 3 for Symbian SQL Database
        -no-sql-tds       # Sybase Adaptive Server
        $(qt_enable mysql sql-mysql plugin "")
        $(option mysql      && echo "-I/usr/$(exhost --target)/include/mysql -L/usr/$(exhost --target)/lib/mysql/")
        $(qt_enable postgresql sql-psql plugin "")
        $(option postgresql && echo "-I/usr/$(exhost --target)/include/postgresql/server/")
        $(qt_enable sqlite sql-sqlite plugin "" -system-sqlite)
    )

    # media formats
    myconf+=(
        -svg
        -system-libjpeg
        -system-libmng
        -system-libpng
        $(qt_enable tiff libtiff system)
    )

    # optional parts
    myconf+=(
        -make libs
        -make tools
        -make translations
        $(qt_build demos)
        $(qt_build doc docs)
        $(qt_build examples)
    )

    # Set {C,CXX,LD}FLAGS.
    # Do not link with -rpath (Gentoo bug #75181).
    edo sed -e "s:QMAKE_CFLAGS_RELEASE.*=.*:QMAKE_CFLAGS_RELEASE=${CFLAGS}:" \
            -e "s:QMAKE_CXXFLAGS_RELEASE.*=.*:QMAKE_CXXFLAGS_RELEASE=${CXXFLAGS}:" \
            -e "s:QMAKE_LFLAGS_RELEASE.*=.*:QMAKE_LFLAGS_RELEASE=${LDFLAGS}:" \
            -e "/CONFIG/s:$: nostrip:" \
            -e "s:QMAKE_RPATH.*=.*:QMAKE_RPATH=:" \
            -e "s:X11R6/::" \
            -i "${WORK}"/mkspecs/$(qt_mkspecs_dir)/qmake.conf

    # use gcc for linking
    unset LD

    qconf4 "${myconf[@]}"

    edo sed -e "s:CXXFLAGS.*=:CXXFLAGS=${CPPFLAGS} ${CXXFLAGS} ${ASFLAGS} :" \
            -e "s:LFLAGS.*=:LFLAGS=${CFLAGS} ${LDFLAGS} :" \
            -i "${WORK}"/qmake/Makefile.unix
}

qt-4_src_compile() {
    # http://bugreports.qt-project.org/browse/QTBUG-5471
    LD_LIBRARY_PATH="${WORK}/lib" default
}

qt-4_src_install() {
    default
    local host=$(exhost --target)

    # remove build dir from libraries
    edo sed -i -e "s:${WORK}/lib:/usr/${host}/lib/qt4:g" "${IMAGE}"/usr/${host}/lib/qt4/{*.la,*.prl,pkgconfig/*.pc}

    # move pkgconfig files out of qt4/ dir
    edo mv "${IMAGE}"/usr/${host}/lib/qt4/pkgconfig "${IMAGE}"/usr/${host}/lib

    if option phonon ; then
        edo rm -rf "${IMAGE}"/usr/${host}/lib/qt4/libphonon*
        edo rm -rf "${IMAGE}"/usr/${host}/lib/pkgconfig/phonon.pc
        edo rm -rf "${IMAGE}"/usr/${host}/include/qt4/phonon
        edo rm "${IMAGE}"/usr/${host}/lib/qt4/plugins/designer/libphononwidgets.so
    fi

    # install symlinks to Qt4 versions of executables that Qt3 also provides. Some buildsystems would fail to find the Qt4 versions without these
    for i in assistant designer linguist lrelease lupdate moc qmake $(option qt3support && echo qtconfig) uic ; do
        [[ -e ${IMAGE}/usr/${host}/bin/${i} ]] || die "/usr/${host}/bin/${i} does not exist in ${IMAGE}"
        dosym ${i} /usr/${host}/bin/${i}-qt4
    done

    keepdir /etc/qt4

    hereenvd 44qt4 <<EOF
LDPATH="/usr/host/lib/qt4"
EOF

    # Also install an env.d file for cross-compiled, runnable arches. Currently
    # only i686 on x86_64. In the future this would benefit from something like
    # exhost --is-runnable.
    if [[ $(exhost --build) == x86_64-pc-linux-gnu ]] ; then
        if [[ $(exhost --target) == i686-pc-linux-gnu ]] ; then
            hereenvd 44qt4-$(exhost --target) <<EOF
LDPATH="/usr/$(exhost --target)/lib/qt4"
EOF
        fi
    fi

    insinto /usr/share/pixmaps
    doins tools/designer/src/designer/images/designer.png
    newins tools/linguist/linguist/images/icons/linguist-128-32.png linguist.png

    insinto /usr/share/applications
    hereins designer.desktop <<EOF
[Desktop Entry]
Type=Application
Name=Qt Designer
GenericName=GUI Designer
Comment=Design GUIs for Qt applications
Exec=designer
Icon=designer
Categories=Qt;Development;GUIDesigner;
MimeType=application/x-designer;
EOF
    hereins linguist.desktop <<EOF
[Desktop Entry]
Type=Application
Name=Qt Linguist
GenericName=GUI Translation Tool
Comment=Add translations to Qt applications
Exec=linguist
Icon=linguist
Categories=Qt;Development;Translation;
MimeType=application/x-linguist;
EOF

    if option examples ; then
        # The examples might add some empty dirs. Since the examples are additional documentation,
        # there shouldn't be any empty dirs we might want to keep, so we remove them all in one go.
        edo find "${IMAGE}"/usr/share/qt4/examples -type d -empty -delete
    fi
}

